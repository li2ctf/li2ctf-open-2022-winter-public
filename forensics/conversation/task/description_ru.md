# Conversation
Автор: [@sultanowskii](http://t.me/sultanowskii)

Спецслужбы Великого Султана засекли, как 5up3r_U53r_2004 и некое третье лицо разговаривали по незащищенному tcp-протоколу. Время для анализа!

[conversation.pcapng](https://drive.google.com/file/d/1uUHS1C6W6FywrmZBKP6jhh2E7ho7PDmV/view?usp=sharing)
