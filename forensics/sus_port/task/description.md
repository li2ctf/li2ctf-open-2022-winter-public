# January 2022 "منفذ مشبوه" Incident
Author: [@sultanowskii](http://t.me/sultanowskii)

Something's wrong on Shah Nurull's server. Recently he discovered some open port, but he doesn't remember he deployed some application with this port. Luckily, he records all the traffic on his PCs (they all considered as one PC, so don't mind same local IP). Therefore he possesses a traffic dump. He asks for the help with investigation - try to find an evidence that his server got hacked.

Of course, no access to the server is provided - condfidential and sensetive data is stored on it.

حظا سعيدا!

[sus_port.pcapng](https://drive.google.com/file/d/1zJO4Br5WnZaBSgtghGDUQyKy38iQVdvj/view?usp=sharing)
