# Афые Ензштп writeup

Given text is an english plaintext typed on russian layout. Using our hands or some online service (like [this](https://gsgen.ru/tools/perevod-raskladki-online/)), we can "decode" the message.

The decoded message is [here](src/orig.txt)

Flag: `li2CTF{_1_4m_7MP_p455w0rD!}`
