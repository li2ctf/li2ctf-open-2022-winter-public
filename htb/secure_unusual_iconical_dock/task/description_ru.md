# Secure Unusual Iconical Dock
Автор: [@sultanowskii](http://t.me/sultanowskii)

Вы стоите около самого безопасного места в нашем мире - пристани Аст'рда Лиманы'. Не делайте резких движений, будьте расторопны и внимательны, и, быть может, это место откроет вам свои тайны.

`ssh solver@ctf.li2sites.ru -p 21005`

password: `b6DYaqVcu2`
