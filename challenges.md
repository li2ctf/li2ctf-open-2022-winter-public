# Challenges
Overall: **28**

| Name                                                                                      | Difficulty| Cost                | Solved |
|-------------------------------------------------------------------------------------------|-----------|---------------------|--------|
| [Laba Altakhmine](crypto/laba_altakhmine/task/description.md)                             | crypto    | 944                 | 6      |
| [Афые Ензштп](forensics/fast_typing/task/description.md)                                  | forensics | 775                 | 11     |
| [Conversation](forensics/conversation/task/description.md)                                | forensics | 775                 | 11     |
| [El Ephant's Tool](forensics/el_ephant's_tool/task/description.md)                        | forensics | 1000                | 1      |
| [The "Sus Port" Incident](forensics/sus_port/task/description.md)                         | forensics | 1000                | 1      |
| [Snake Temple](htb/snake_temple/task/description.md)                                      | htb       | 890                 | 8      |
| [Secure Unususal Iconical Dock](htb/secure_unusual_iconical_dock/task/description.md)     | htb       | 991                 | 3      |
| [Library](htb/library/task/description.md)                                                | htb       | 964                 | 5      |
| [Sanity Check](misc/sanity_check/task/description.md)                                     | misc      | 775                 | 11     |
| [Gitshit](misc/gitshit/task/description.md)                                               | misc      | 964                 | 5      |
| [Math Giveaway](misc/math_giveaway/task/description.md)                                   | misc      | 944                 | 6      |
| [Cleaner](ppc/cleaner/task/description.md)                                                | ppc       | 856                 | 9      |
| [Wizzdomzz](pwn/wizzdomzz/task/description.md)                                            | pwn       | 980                 | 4      |
| [Greeter](pwn/greeter/task/description.md)                                                | pwn       | 1000                | 1      |
| [Grand Bazaar](pwn/grand_bazaar/task/description.md)                                      | pwn       | 991                 | 3      |
| [5CR34M](pwn/5CR34M/task/description.md)                                                  | pwn       | 1000                | 0      |
| [Present 1](reverse/present1/task/description.md)                                         | reverse   | 818                 | 10     |
| [Present 2](reverse/present2/task/description.md)                                         | reverse   | 991                 | 3      |
| [Present 3](reverse/present3/task/description.md)                                         | reverse   | 1000                | 1      |
| [Ritual](reverse/ritual/task/description.md)                                              | reverse   | 1000                | 1      |
| [Present 4](reverse/present3/task/description.md)                                         | reverse   | 1000                | 1      |
| [Cookies](web/cookies/task/description.md)                                                | web       | 775                 | 11     |
| [Robots](web/robots/task/description.md)                                                  | web       | 818                 | 10     |
| [Big Grey Rock](web/big_grey_rock/task/description.md)                                    | web       | 728                 | 12     |
| [Secure vault v3.0](web/secure_vault_v3.0/task/description.md)                            | web       | 856                 | 9      |
| [Secure vault v4.0](web/secure_vault_v4.0/task/description.md)                            | web       | 856                 | 9      |
| [ElectroT](web/electroT/task/description.md)                                              | web       | 1000                | 0      |
| [oxxxymiron](web/oxxxymiron/task/description.md)                                          | web       | 1000                | 0      |
