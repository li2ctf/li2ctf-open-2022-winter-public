# Grand Bazaar
Author: [@sultanowskii](http://t.me/sultanowskii)

Shah Izz Ad-Deen always says: "Everybody should have a rest from time to time". They have given you a day off, and offered to visit the biggest bazaar of The Great Sultan's lands. Shah also have given you this 100 gold coins Enjoy your holiday! أتمنى لك يوما سعيدا!

`nc ctf.li2sites.ru 21016`

[grand_bazaar.zip](https://drive.google.com/file/d/1Cwm03ciysTRpcBLfRRpBSsT3mTGWZtWO/view?usp=sharing)
