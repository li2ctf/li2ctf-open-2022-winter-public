# Greeter
Author: [@sultanowskii](http://t.me/sultanowskii)

Greet the Great Sultan correctly and get their blessing!

`nc ctf.li2sites.ru 21001`

[greeter.zip](https://drive.google.com/file/d/1CQApbPSyk9pcnrx9T4RxUAQqnEkqFpC1/view?usp=sharing)
