# Greeter
Автор: [@sultanowskii](http://t.me/sultanowskii)

Поздоровайтесь с Великим Султаном правильно и получите его благословение!

`nc ctf.li2sites.ru 21001`

[greeter.zip](https://drive.google.com/file/d/1CQApbPSyk9pcnrx9T4RxUAQqnEkqFpC1/view?usp=sharing)
