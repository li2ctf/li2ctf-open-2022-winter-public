# Cookies writeup
Name of the task leads us to check cookies. The **secret** cookie contains this string:

```
bGkyQ1RGezRsbF83aDNfYzBkM3J6X2wwdjNfYzAwazEzNV9fZDBuN183aDN5P30=
```

With the help of some online services we may detect that this is base64-encrypted string. Decode it and get the flag.

Flag: `li2CTF{4ll_7h3_c0d3rz_l0v3_c00k135__d0n7_7h3y?}`
