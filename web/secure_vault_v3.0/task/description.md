# Secure vault v3.0
Author: [@sultanowskii](http://t.me/sultanowskii)

After half a year we are ready to present you a **Secure vault** v3.0! We transferred the development to our east colleagues, they managed to fix the SQL queries.

Additionally, they moved error handler to client side, not to waste your internet traffic only for "Incorrect password" sign lol. Cool fellas, I want to say!

username: `user` password: `user`

http://ctf.li2sites.ru:21010
