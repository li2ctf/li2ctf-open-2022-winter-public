# Hints
## Here is an interesting thought:
`main()` is also called from somewhere. But where from?

---

## `exit()`, `int`, reading from `NULLPTR`
Somebody really doesn't want to make you execute this function
