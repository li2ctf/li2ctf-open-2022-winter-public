# Hints
## Always `false` in if?
No need to make it true, it is much easier to change the condition.
