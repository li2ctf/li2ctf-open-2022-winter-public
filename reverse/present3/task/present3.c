#include <stdio.h>
#include <stdlib.h>

void __attribute__ ((optimize(3))) printFlag() {
	/*
		Reverse challenge moment: no flags in source code!
	*/
}

int main() {
	printFlag();
	return 0;
}
