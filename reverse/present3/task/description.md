# Present 3
Author: [@sultanowskii](http://t.me/sultanowskii)

Third present to The Great Sultan is a large building with an enormous number of archways and entrances. There are some rumours that a real treasure stored in the middle of it, but no servant succeeded in getting to it.

[present3.zip](https://drive.google.com/file/d/1wpDE5phY0dWR7K-1iCjS8TWSVBdlMPay/view?usp=sharing)
